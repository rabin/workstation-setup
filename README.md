Role Name
=========

workstation_setup

Requirements
------------

- git
- ansible


Role Variables
--------------

See defaults/main.yml


Example Playbook
----------------

    ---
    - hosts: localhost

      vars:
        my_user: foo
        install_google_chrome: True
        install_vivaldi_browser: True
        install_vscode: True
        install_nvidia_driver: False
        upgrade_all_packages: True

      remote_user: root
      become: true

      roles:
        - workstation_setup


License
-------

BSD